---
- name: gather required facts
  ansible.builtin.setup:
    gather_subset:
      - distribution
      - distribution_major_version

- name: set up bullseye-backports to enable zstd support in reprepro
  block:
    - name: copy bullseye-backports.sources
      ansible.builtin.copy:
        content: |
          deb http://deb.debian.org/debian bullseye-backports main
        dest: /etc/apt/sources.list.d/bullseye-backports.list
      register: bullseye_backports_sources

    - name: update apt cache
      ansible.builtin.apt:
        update_cache: yes
      when: bullseye_backports_sources.changed

    - name: set target release for reprepro
      ansible.builtin.set_fact:
        reprepro_default_release: bullseye-backports

  when: ansible_facts['distribution'] == "Debian" and ansible_facts['distribution_major_version'] | int == 11

- name: install reprepro package
  ansible.builtin.apt:
    pkg: reprepro
    default_release: "{{ reprepro_default_release|default('') }}"

- name: install required packages
  ansible.builtin.apt:
    pkg:
      - gnupg

- name: create default user
  ansible.builtin.user:
    name: '{{ reprepro_user }}'
    create_home: yes
  register: user_data

- name: create all reprepro directories
  ansible.builtin.file:
    path: '{{ reprepro_dir }}/{{ item }}'
    state: directory
    mode: 0755
    owner: '{{ reprepro_user }}'
    group: '{{ reprepro_user }}'
  loop:
    - conf
    - db
    - lists
    - logs
    - bin

- name: create repository related directories
  ansible.builtin.file:
    path: '{{ reprepro_repositorydir }}/{{ item }}'
    state: directory
    mode: 0755
    owner: '{{ reprepro_user }}'
    group: '{{ reprepro_user }}'
  loop:
    - dists
    - pool

- name: create symlinks for repository related directories
  ansible.builtin.file:
    path: '{{ reprepro_dir }}/{{ item }}'
    state: link
    src: '{{ reprepro_repositorydir }}/{{ item }}'
    owner: '{{ reprepro_user }}'
    group: '{{ reprepro_user }}'
  loop:
    - dists
    - pool
  when: reprepro_repositorydir != reprepro_dir

- name: create service directories with group write
  ansible.builtin.file:
    path: '{{ lookup("vars", item) }}'
    state: directory
    mode: 0775
    owner: '{{ reprepro_user }}'
    group: '{{ reprepro_user }}'
  loop:
    - reprepro_tmpdir
    - reprepro_logdir
    - reprepro_morguedir

- name: create artifact directory
  ansible.builtin.file:
    path: '{{ reprepro_artifactdir }}'
    state: directory
    mode: 0755
    owner: '{{ reprepro_user }}'
    group: '{{ reprepro_user }}'
  when: reprepro_artifactdir != reprepro_dir

- name: create GPG key
  ansible.builtin.include_role:
    name: freexian.debian_infrastructure.gpg
    tasks_from: genkey
  vars:
    gpg_user: '{{ reprepro_user }}'
    gpg_key_realname: '{{ reprepro_gpg_key_realname }}'
    gpg_key_email: '{{ reprepro_gpg_key_email }}'
    gpg_key_type: RSA
    gpg_key_length: 4096
    gpg_key_usage: sign
    gpg_expire_date: '5y'
  when: reprepro_enable_signature

- name: export archive key (binary keyring)
  ansible.builtin.shell:
    cmd: 'gpg --export-options export-minimal --export {{ reprepro_gpg_key_email }} > {{ reprepro_artifactdir }}/archive-key.gpg'
    creates: '{{ reprepro_artifactdir }}/archive-key.gpg'
  become: yes
  become_user: '{{ reprepro_user }}'
  when: reprepro_enable_signature

- name: export archive key (ascii armored)
  ansible.builtin.shell:
    cmd: 'gpg --armor --export-options export-minimal --export {{ reprepro_gpg_key_email }} > {{ reprepro_artifactdir }}/archive-key.asc'
    creates: '{{ reprepro_artifactdir }}/archive-key.asc'
  become: yes
  become_user: '{{ reprepro_user }}'
  when: reprepro_enable_signature

- name: create configuration files from templates
  ansible.builtin.template:
    src: '{{ reprepro_templates[item] }}'
    dest: '{{ reprepro_dir }}/conf/{{ item }}'
    owner: '{{ reprepro_user }}'
  loop:
    - distributions
    - updates
    - pulls
    - uploaders
    - incoming
    - override
  when: 'item in reprepro_templates'
  notify: reprepro export
  tags:
    - buildd_data

- name: place templatized repository tools
  ansible.builtin.template:
    src: 'reprepro-{{ item }}.j2'
    dest: '{{ reprepro_dir }}/bin/{{ item }}'
    mode: 0755
    owner: '{{ reprepro_user }}'
  loop:
    - trigger-build

- name: setup incoming directory
  ansible.builtin.include_tasks:
    file: incoming.yml
    apply:
      tags:
        - incoming
  when: reprepro_enable_incoming
