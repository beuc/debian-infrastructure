debci\_worker
=============

Install and configure debci-worker. This includes the setup of workers
instances. The `debci-worker@*.service` is repurposed and one is created for
each configured combination of architecture, suite and backend.

This role assumes the presence of a working AMQP server. Either run the
`rabbitmq` role or configure a `debci_amqp_server` in the `debci_common` role.

License
-------

MIT

Author Information
------------------
Sébastien Delafond <sdelafond@gmail.com>
Helmut Grohne <helmut@freexian.com>
