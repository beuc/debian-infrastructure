# Debusine-common role

Role with Ansible tasks that are needed from debusine roles
(such as `debusine_server` or `debusine_worker`).

# Role variables

## Variables
* `debusine_use_snapshot_repository`: if true use the latest debusine devel build.
  Otherwise, use the debusine available on deb.freexian.com

# License

MIT

# Author Information

Carles Pina i Estany <carles@pina.cat>
