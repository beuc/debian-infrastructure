# Ansible Collection - freexian.debian_infrastructure

This collection provides many roles to deploy Debian build infrastructure.

Among the roles there are:
- `debci_common`: shared role for `debci` components
- `debci_master`: set up all `debci` components except for actually running tests
- `debci_worker`: set up a `debci` worker, i.e. a machine running tests
- `distro_info`: basic role exporting reusable data about Debian-based
  distributions
- `rabbitmq`: set up a `RabbitMQ` server as a prerequisite for `debci`
- `sbuild`: set up build chroots for use with sbuild
- `reprepro`: set up a package repository with an incoming queue
- `dirvish`: set up some backups with the dirvish tool
- `rebuildd`: set up a package build daemon with rebuildd (to be deprecated
  because rebuildd is no longer maintained)
- `distro_tracker`: set up a distro-tracker instance
- `mirror_status`: set up a mirror-status instance
- `debusine_server`: set up a Debusine server instance
- `debusine_worker`: set up a Debusine worker

The following roles should not be considered for public consumption, they
are only used internally by other roles:
- `gpg`: create and import GPG keys (used by the reprepro role)

# Authors

This collection is maintained by the Debian developers behind Freexian:
https://www.freexian.com
